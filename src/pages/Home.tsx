import { TypeAnimation } from "react-type-animation";
import { SimpleBtn } from "../components";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../app/store";
import { setUseanimation } from "../app/slices/homeSlice";

function Home() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { useanimation } = useSelector((state: RootState) => state.Home);
  const [showbtn, setShowbtn] = useState(false);
  useEffect(() => {
    useanimation
      ? setTimeout(() => {
          setShowbtn(true);
        }, 13000)
      : setShowbtn(true);
  }, []);

  const gonext = (name: string) => {
    dispatch(setUseanimation(false));
    navigate(name);
  };
  return (
    <div className="flex flex-col">
      {useanimation ? (
        <>
          <TypeAnimation
            className="text-center font-bold mb-4 my-8"
            sequence={[
              "This mini-project is solely for you to see my coding style",
            ]}
            cursor={false}
            wrapper="div"
            speed={50}
            style={{ fontSize: "2em" }}
            repeat={0}
          />
          <TypeAnimation
            className="text-center font-bold my-4"
            sequence={[
              5000,
              "Run the code below and then click on the next button",
            ]}
            cursor={false}
            wrapper="div"
            speed={50}
            style={{ fontSize: "2em" }}
            repeat={0}
          />

          <TypeAnimation
            className="text-center font-bold text-[#3a86ff] my-4"
            sequence={[10000, "npm run server"]}
            cursor={false}
            wrapper="div"
            speed={50}
            style={{ fontSize: "2em" }}
            repeat={0}
          />
        </>
      ) : (
        <>
          <TypeAnimation
            className="text-center text-[#3a86ff] font-bold  mb-8 mt-14"
            sequence={[
              "Thank you for taking the time to look at my mini project.",
            ]}
            cursor={false}
            wrapper="div"
            speed={50}
            style={{ fontSize: "2em" }}
            repeat={0}
          />
        </>
      )}

      {showbtn ? (
        <SimpleBtn
          type="button"
          onClick={() => gonext("users")}
          className="mx-auto my-8 text-lg"
        >
          next
        </SimpleBtn>
      ) : null}
    </div>
  );
}

export default Home;
