
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Users from "./pages/Users";
import Edituser from "./pages/Edituser";

function App() {
  return (
<div className="w-full">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/users" element={<Users />} />
          <Route path="/edituser/:id" element={<Edituser />} />
        </Routes>
      </BrowserRouter>
      </div>
   
  );
}

export default App;
